#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
   FILE *inp_file = fopen(argv[1], "r");
   int width = atoi(argv[2]);
   int height = atoi(argv[3]);
   int array_size = width * height;

   float *inp_arr = calloc(array_size, sizeof(float));
   size_t read = fread(inp_arr, sizeof(float), array_size, inp_file);

   fclose(inp_file);

   for (unsigned int i = 0; i < array_size; ++i) {
      printf("element %d: %f\n", i, inp_arr[i]);
   }

   free(inp_arr);

   return 0;
}
