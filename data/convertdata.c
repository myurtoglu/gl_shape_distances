#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
   FILE *inp_file = fopen(argv[1], "r");
   int width = atoi(argv[2]);
   int height = atoi(argv[3]);
   int array_size = width * height;

   float *inp_arr = calloc(array_size, sizeof(float));
   size_t read = fread(inp_arr, sizeof(float), array_size, inp_file);
   FILE *output_file = fopen(argv[4], "w");

   for (unsigned int i = 0; i < array_size; ++i) {
      if (inp_arr[i] <= 0) {
         inp_arr[i] = 0;
      }
      else {
         inp_arr[i] = 1000;
      }
   }

   size_t wrote = fwrite(inp_arr, sizeof(float), array_size, output_file);

   fclose(output_file);
   fclose(inp_file);
   free(inp_arr);

   return 0;
}
