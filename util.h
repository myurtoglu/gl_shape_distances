#ifndef UTIL_H
#define UTIL_H

// general exit with message function
void   my_error(const char* msg);

// python style modulus
double mod2(double x, double y);

// if not integer, round up
int div_up(int a, int b);

#endif
