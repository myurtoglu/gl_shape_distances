#include <GL/glew.h>
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include "util.h"
#include "shape.h"

// CUDA Runtime, Interop, and includes
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>
#include <vector_types.h>
#include <vector_functions.h>
#include <driver_functions.h>

// CUDA utilities
#include <helper_cuda.h>
#include <helper_cuda_gl.h>

// Helper functions
#include <helper_cuda.h>
#include <helper_functions.h>
#include <helper_timer.h>

#define MAX_H_RES 1920
#define MAX_V_RES 1080
#define H_RES      1600
#define V_RES      900
#define DIST_COEF  5.0f
#define ROT_COEF   6.0f
#define REC_WIDTH  150 // rectangle shapes changed later in main()
#define REC_HEIGHT 75

// Kernel launch parameters
dim3 blockSize(32, 32);
dim3 gridSize;

bool isClose = false;
bool draw_outlines = true;
bool draw_color_background = true;
bool collision = false;
char info_bar[240];
char fps[40];

float *inp_arr1;
float *inp_arr2;
int width;
int height;
size_t array_size;

// fps timing
StopWatchInterface *timer = 0; // Initialize FPS timer
int fpsCount = 0;        // FPS count for averaging
int fpsLimit = 1;        // FPS limit for sampling

// texture and pixel objects
GLuint pbo1 = 0;     // OpenGL pixel buffer object
GLuint tex1 = 0;     // OpenGL texture object

// CUDA Graphics Resource (to transfer PBO)
struct cudaGraphicsResource *cuda_pbo_resource1;

GLuint pbo2 = 0;
GLuint tex2 = 0;

struct cudaGraphicsResource *cuda_pbo_resource2;
cudaTextureObject_t texobj = 0;

Shape s1 = {H_RES / 5.0f, V_RES / 5.0f, 0, 0, 0.0f, 'r'};
Shape s2 = {4.0f * H_RES / 5, 4.0f * V_RES / 5, 0, 0, 0.0f,
            'p'};
// Define external helper function for calling kernel
// (kernels MUST be called within the *.cu file)
// Helper functions are used to indirectly invoke a kernel
// launch from a non-*.cu file
extern "C" void render_kernel(dim3 gridSize, dim3 blockSize, uchar4 *d_output,
                              int imageW, int imageH, float *inp_arr);

extern "C" void render_collision_kernel(dim3 gridSize, dim3 blockSize,
                                        uchar4 *d_output, int imageW,
                                        int imageH, float *inp_arr1,
                                        float *inp_arr2,
                                        cudaTextureObject_t texobj,
                                        float arr_pos_x, float arr_pos_y,
                                        float arr_heading, float texobj_pos_x,
                                        float texobj_pos_y,
                                        float texobj_heading, bool *collision,
                                        bool isClose);

void computeFPS()
{
   fpsCount++;

   if (fpsCount == fpsLimit)
   {
      float ifps = 1.f / (sdkGetAverageTimerValue(&timer) / 1000.f);
      sprintf(fps, ", FPS: %3.1f", ifps);
      fpsCount = 0;

      fpsLimit = (int)fmax(1.f, ifps);
      sdkResetTimer(&timer);
   }
}

// render image using CUDA
void render()
{
    // map PBO to get CUDA device pointer
   uchar4 *d_output1;
   uchar4 *d_output2;
    // map PBO to get CUDA device pointer
   checkCudaErrors(cudaGraphicsMapResources(1, &cuda_pbo_resource1, 0));
   checkCudaErrors(cudaGraphicsMapResources(1, &cuda_pbo_resource2, 0));
   size_t num_bytes;
   // Release CUDA pointer from OpenGL for CUDA modification
   checkCudaErrors(cudaGraphicsResourceGetMappedPointer((void **)&d_output1,
                                                        &num_bytes,
                                                        cuda_pbo_resource1));

   checkCudaErrors(cudaGraphicsResourceGetMappedPointer((void **)&d_output2,
                                                        &num_bytes,
                                                        cuda_pbo_resource2));

   // clear image
   checkCudaErrors(cudaMemset(d_output1, 0, width*height*4));
   checkCudaErrors(cudaMemset(d_output2, 0, width*height*4));

   // call CUDA kernel, writing results to PBO
   render_kernel(gridSize, blockSize, d_output1, width, height, inp_arr1);
   // render_kernel(gridSize, blockSize, d_output2, width, height, inp_arr2);
   collision = false;
   render_collision_kernel(gridSize, blockSize, d_output2, width, height,
                           inp_arr2, inp_arr1, texobj, s2.pos_x, s2.pos_y,
                           s2.heading, s1.pos_x, s1.pos_y, s1.heading,
                           &collision, isClose);
   getLastCudaError("kernel failed");
   // Release PBO for OpenGL display
   checkCudaErrors(cudaGraphicsUnmapResources(1, &cuda_pbo_resource1, 0));
   checkCudaErrors(cudaGraphicsUnmapResources(1, &cuda_pbo_resource2, 0));
}

void draw_rect(float center_x, float center_y, float width, float height,
               char color)
{
   switch(color) {
      case 'r':
         glColor3f(1.0f, 0.0f, 0.0f);
         break;
      case 'g':
         glColor3f(0.0f, 1.0f, 0.0f);
         break;
      case 'b':
         glColor3f(0.0f, 0.0f, 1.0f);
         break;
      case 'y':
         glColor3f(1.0f, 1.0f, 0.0f);
         break;
      case 'p':
         glColor3f(1.0f, 0.0f, 1.0f);
         break;
      default:
         glColor3f(1.0f, 1.0f, 1.0f);
   }

   float a_x = (center_x - width / 2.0f);
   float a_y = (center_y - height / 2.0f);
   float b_x = (center_x - width / 2.0f);
   float b_y = (center_y + height / 2.0f);
   float c_x = (center_x + width / 2.0f);
   float c_y = (center_y + height / 2.0f);
   float d_x = (center_x + width / 2.0f);
   float d_y = (center_y - height / 2.0f);

   glBegin(GL_LINE_LOOP);
      glVertex2f(a_x, a_y);
      glVertex2f(b_x, b_y);
      glVertex2f(c_x, c_y);
      glVertex2f(d_x, d_y);
   glEnd();
}

void draw_line(Point *pt1, Point *pt2, char color)
{
   if (!pt1 || !pt2)
      my_error("Points not allocated correctly");

   switch(color) {
      case 'r':
         glColor3f(1.0f, 0.0f, 0.0f);
         break;
      case 'g':
         glColor3f(0.0f, 1.0f, 0.0f);
         break;
      case 'b':
         glColor3f(0.0f, 0.0f, 1.0f);
         break;
      case 'y':
         glColor3f(1.0f, 1.0f, 0.0f);
         break;
      case 'p':
         glColor3f(1.0f, 0.0f, 1.0f);
         break;
      default:
         glColor3f(1.0f, 1.0f, 1.0f);
   }

   float a_x = pt1->x;
   float a_y = pt1->y;
   float b_x = pt2->x;
   float b_y = pt2->y;

   glBegin(GL_LINES);
      glVertex2f(a_x, a_y);
      glVertex2f(b_x, b_y);
   glEnd();
}

void draw_texture(float center_x, float center_y, float width, float height)
{
   float a_x = (center_x - width / 2.0f);
   float a_y = (center_y - height / 2.0f);
   float b_x = (center_x - width / 2.0f);
   float b_y = (center_y + height / 2.0f);
   float c_x = (center_x + width / 2.0f);
   float c_y = (center_y + height / 2.0f);
   float d_x = (center_x + width / 2.0f);
   float d_y = (center_y - height / 2.0f);

   glEnable(GL_TEXTURE_2D);
      glBegin(GL_QUADS);
         glTexCoord2f(0.0f, 0.0f);
         glVertex2f(a_x, a_y);
         glTexCoord2f(0.0f, 1.0f);
         glVertex2f(b_x, b_y);
         glTexCoord2f(1.0f, 1.0f);
         glVertex2f(c_x, c_y);
         glTexCoord2f(1.0f, 0.0f);
         glVertex2f(d_x, d_y);
      glEnd();
   glDisable(GL_TEXTURE_2D);
}

void display()
{
   // Starting the timer for displaying FPS
   sdkStartTimer(&timer);

   // Call render function
   render();

   if (isClose && draw_color_background)
      glClearColor (18.0/255, 150.0/255, 59.0/255, 0.0);
   else
      glClearColor (0.0, 0.0, 0.0, 0.0);

   glClear(GL_COLOR_BUFFER_BIT);
   glLineWidth(2.0f);

   // displaying shape 1
   if (draw_outlines) {
      glPushMatrix();
         glTranslatef(s1.pos_x, s1.pos_y, 0.0f);
         glRotatef(s1.heading, 0.0f, 0.0f, 1.0f);
         draw_rect(0, 0, s1.width, s1.height, s1.color);
      glPopMatrix();
   }

   // resetting the color
   glColor3f(1.0f, 1.0f, 1.0f);

   // displaying texture 1
   glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, pbo1);
   glBindTexture(GL_TEXTURE_2D, tex1);
   glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA,
                   GL_UNSIGNED_BYTE, 0);
   glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, 0);

   // glMatrixMode(GL_TEXTURE);
   glPushMatrix();
      glTranslatef(s1.pos_x, s1.pos_y, 0.0f);
      glRotatef(s1.heading, 0.0f, 0.0f, 1.0f);
      draw_texture(0.0f, 0.0f, width, height);
   glPopMatrix();


   glBindTexture(GL_TEXTURE_2D, 0); // Clear texture by setting to zero


   // displaying shape 2
   if (draw_outlines) {
      glPushMatrix();
         glTranslatef(s2.pos_x, s2.pos_y, 0.0f);
         glRotatef(s2.heading, 0.0f, 0.0f, 1.0f);
         draw_rect(0, 0, s2.width, s2.height, s2.color);
      glPopMatrix();
   }

   // resetting the color
   glColor3f(1.0f, 1.0f, 1.0f);


   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
   glEnable(GL_BLEND);
   // displaying texture 2
   glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, pbo2);
   glBindTexture(GL_TEXTURE_2D, tex2);
   glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA,
                   GL_UNSIGNED_BYTE, 0);
   glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, 0);

   glPushMatrix();
      glTranslatef(s2.pos_x, s2.pos_y, 0.0f);
      glRotatef(s2.heading, 0.0f, 0.0f, 1.0f);
      draw_texture(0.0f, 0.0f, width, height);
   glPopMatrix();

   glBindTexture(GL_TEXTURE_2D, 0); // Clear texture by setting to zero

   // drawing the closest distance line
   Point *closest_pts = my_get_closest_pts(&s1, &s2);
   if (!closest_pts)
      my_error("There was an error in getting the closest points");

   if (draw_outlines) {
      draw_line(&closest_pts[0], &closest_pts[1], 'y');
   }

   float pr_dist = my_pt_dist(&closest_pts[0], &closest_pts[1]);

   if (pr_dist <= 5.0f)
      isClose = true;
   if (pr_dist >= width / 2.0f)
      isClose = false;

   free(closest_pts);
   closest_pts = NULL;

   sprintf(info_bar, "Bounding Box Distance: %3.1f", isClose ? 0 : pr_dist);
   char new_info[30];
   sprintf(new_info, ", Collision: %s",
           collision ? "\xE2\x9C\x85" : "\xE2\x9D\x8C");
   strcat(info_bar, new_info);
   memset(new_info,' ', 30);
   sprintf(new_info, ", Draw Outlines (/): %s",
           draw_outlines ? "\xE2\x9C\x85" : "\xE2\x9D\x8C");
   strcat(info_bar, new_info);
   memset(new_info,' ', 30);
   sprintf(new_info, ", Change BG when close (.): %s",
           draw_color_background ? "\xE2\x9C\x85" : "\xE2\x9D\x8C");
   strcat(info_bar, new_info);
   strcat(info_bar, fps);

   glutSwapBuffers();
   glutReportErrors();

   sdkStopTimer(&timer);
   computeFPS();
   glutSetWindowTitle(info_bar);
}

void init()
{
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluOrtho2D(0, H_RES, V_RES, 0);
}

void keyboard(unsigned char key, int x, int y)
{
   switch (key) {
      case 27:
         exit(0);
         break;

      case 'w':
         s1.pos_y = mod2(s1.pos_y - DIST_COEF, V_RES);
         break;

      case 's':
         s1.pos_y = mod2(s1.pos_y + DIST_COEF, V_RES);
         break;

      case 'd':
         s1.pos_x = mod2(s1.pos_x + DIST_COEF, H_RES);
         break;

      case 'a':
         s1.pos_x = mod2(s1.pos_x - DIST_COEF, H_RES);
         break;

      case 'e':
         s1.heading = mod2(s1.heading + ROT_COEF, 360.0);
         break;

      case 'q':
         s1.heading = mod2(s1.heading - ROT_COEF, 360.0);
         break;

      case 'i':
         s2.pos_y = mod2(s2.pos_y - DIST_COEF, V_RES);
         break;

      case 'k':
         s2.pos_y = mod2(s2.pos_y + DIST_COEF, V_RES);
         break;

      case 'l':
         s2.pos_x = mod2(s2.pos_x + DIST_COEF, H_RES);
         break;

      case 'j':
         s2.pos_x = mod2(s2.pos_x - DIST_COEF, H_RES);
         break;

      case 'o':
         s2.heading = mod2(s2.heading + ROT_COEF, 360.0);
         break;

      case 'u':
         s2.heading = mod2(s2.heading - ROT_COEF, 360.0);
         break;

      case 'r':
         s1.width += 5.0f;
         break;

      case 'f':
         s1.width -= 5.0f;
         break;

      case 'p':
         s2.width += 5.0f;
         break;

      case ';':
         s2.width -= 5.0f;
         break;

      case '/':
         draw_outlines = !draw_outlines;
         break;

      case '.':
         draw_color_background = !draw_color_background;
         break;

      default:
         break;
   }

   glutPostRedisplay(); // Refresh screen after making changes
}

void idle()
{
   // Refresh screen if no input from user
   glutPostRedisplay();
}

// Initialize OpenGL window
void initGL(int *argc, char **argv)
{
   // initialize GLUT callback functions
   glutInit(argc, argv);
   glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
   glutInitWindowSize(H_RES, V_RES);
   glutInitWindowPosition((MAX_H_RES - H_RES)/2, (MAX_V_RES - V_RES)/2);
   glutCreateWindow("shape distances");

   glewInit();

   if (!glewIsSupported("GL_VERSION_2_0 GL_ARB_pixel_buffer_object"))
   {
      printf("Required OpenGL extensions missing.");
      exit(EXIT_SUCCESS);
   }
}

// Initialize PBO
void initPixelBuffer()
{
   if (pbo1)
   {
      // unregister this buffer object from CUDA C
      checkCudaErrors(cudaGraphicsUnregisterResource(cuda_pbo_resource1));

      // delete old buffer
      glDeleteBuffersARB(1, &pbo1);
      glDeleteTextures(1, &tex1);
   }

   if (pbo2)
   {
      // unregister this buffer object from CUDA C
      checkCudaErrors(cudaGraphicsUnregisterResource(cuda_pbo_resource2));

      // delete old buffer
      glDeleteBuffersARB(1, &pbo2);
      glDeleteTextures(1, &tex2);
   }

   // create pixel buffer object for display
   glGenBuffersARB(1, &pbo1);
   glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, pbo1);
   glBufferDataARB(GL_PIXEL_UNPACK_BUFFER_ARB, width*height*sizeof(GLubyte)*4,
                   0, GL_STREAM_DRAW_ARB);
   glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, 0);

   // register this buffer object with CUDA
   checkCudaErrors(cudaGraphicsGLRegisterBuffer(&cuda_pbo_resource1, pbo1,
                                                cudaGraphicsMapFlagsWriteDiscard));

   // create texture for display
   glGenTextures(1, &tex1);
   glBindTexture(GL_TEXTURE_2D, tex1);
   glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA,
                GL_UNSIGNED_BYTE, NULL);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
   glBindTexture(GL_TEXTURE_2D, 0);

   // create pixel buffer object for display
   glGenBuffersARB(1, &pbo2);
   glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, pbo2);
   glBufferDataARB(GL_PIXEL_UNPACK_BUFFER_ARB, width*height*sizeof(GLubyte)*4,
                   0, GL_STREAM_DRAW_ARB);
   glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, 0);

   // register this buffer object with CUDA
   checkCudaErrors(cudaGraphicsGLRegisterBuffer(&cuda_pbo_resource2, pbo2,
                                                cudaGraphicsMapFlagsWriteDiscard));

   // create texture for display
   glGenTextures(1, &tex2);
   glBindTexture(GL_TEXTURE_2D, tex2);
   glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA,
                GL_UNSIGNED_BYTE, NULL);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
   glBindTexture(GL_TEXTURE_2D, 0);
}

void exitfunc()
{
   free(inp_arr1);
   inp_arr1 = NULL;
   free(inp_arr2);
   inp_arr2 = NULL;
   // freeCudaBuffers();
   // Destroy texture object
   checkCudaErrors(cudaDestroyTextureObject(texobj));
   texobj = 0;
   // Free device memory
   // cudaFreeArray(cuArray);
   sdkDeleteTimer(&timer);

   if (pbo1)
   {
      checkCudaErrors(cudaGraphicsUnregisterResource(cuda_pbo_resource1));
      glDeleteBuffersARB(1, &pbo1);
      glDeleteTextures(1, &tex1);
   }

   if (pbo2)
   {
      checkCudaErrors(cudaGraphicsUnregisterResource(cuda_pbo_resource2));
      glDeleteBuffersARB(1, &pbo2);
      glDeleteTextures(1, &tex2);
   }
}

int main(int argc, char** argv)
{
   if (argc != 3) {
      my_error(
         "USAGE: ./main.exe <filename1_width.dat> <filename2_width.dat>");
   }
   int length = strlen(argv[1]);
   char width_str[3];
   int j = 0;
   for (int i = length - 7; i < length - 4; ++i) {
      if (argv[1][i] >'9' || argv[1][i] < '0') {
         my_error(
            "USAGE: ./main.exe <filename1_width.dat> <filename2_width.dat>");
      }
      width_str[j++] = argv[1][i];
   }

   width = atoi(width_str);
   height = width;

   s1.width   = width;
   s2.width   = width;
   s1.height  = height;
   s2.height  = height;

   array_size = width * height;
   // Inputting the data

   // const char *filename = "griddata6.dat";
   char filename1[100];
   strcpy (filename1, "./data/");
   strcat (filename1, argv[1]);
   FILE *inp_file1 = fopen(filename1, "r");
   if (inp_file1 == NULL)
      my_error("Could not read the given file.");

   inp_arr1 = (float*) calloc(array_size, sizeof(float));

   if (inp_arr1 == NULL)
      my_error("Could not allocate memory.");

   size_t read = fread(inp_arr1, sizeof(float), array_size, inp_file1);

   if (read == 0)
      my_error("Nothing read.");

   fclose(inp_file1);

   char filename2[100];
   strcpy (filename2, "./data/");
   strcat (filename2, argv[2]);
   FILE *inp_file2 = fopen(filename2, "r");
   if (inp_file2 == NULL)
      my_error("Could not read the given file.");

   inp_arr2 = (float*) calloc(array_size, sizeof(float));
   if (inp_arr2 == NULL)
      my_error("Could not allocate memory.");

   read = fread(inp_arr2, sizeof(float), array_size, inp_file2);
   if (read == 0)
      my_error("Nothing read.");

   fclose(inp_file2);

   gridSize = dim3(div_up(width, blockSize.x), div_up(height, blockSize.y));

   sdkCreateTimer(&timer);

   // OpenGL
   initGL(&argc, argv);
   glutKeyboardFunc(keyboard);
   glutDisplayFunc(display);
   glutIdleFunc(idle);
   init();
   initPixelBuffer();
   atexit(exitfunc);

   glutMainLoop();

   checkCudaErrors(cudaDeviceReset());
   exit(EXIT_SUCCESS);
}
