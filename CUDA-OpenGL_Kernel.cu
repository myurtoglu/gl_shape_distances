// CUDA Helper files
#include <helper_cuda.h>
#include <helper_math.h>
#include <math_constants.h>
#include "util.h"

// Set pixel values for rendering the screen
__global__ void
g_render(uchar4 *d_output, int imageW, int imageH, float *d_inp_arr)
{
	// Pixel/thread mapping
   int x = blockIdx.x*blockDim.x + threadIdx.x;
   int y = blockIdx.y*blockDim.y + threadIdx.y;
	int offset = x + y * imageW; // 1D indexing

   if ((x >= imageW) || (y >= imageH)) return; // Check if within image bounds

   d_output[offset].x = d_inp_arr[offset] > 0 ? 0 : 0;
   d_output[offset].y = d_inp_arr[offset] > 0 ? 0 : 0;
   d_output[offset].z = d_inp_arr[offset] > 0 ? 255 : 0;
   d_output[offset].w = 255;
}

// A little smarter version of g_render, checks for collision with texobj
__global__ void
c_render(uchar4 *d_output, int imageW, int imageH, float *d_inp_arr,
         cudaTextureObject_t texobj, float arr_pos_x, float arr_pos_y,
         float arr_heading, float texobj_pos_x, float texobj_pos_y,
         float texobj_heading, bool *d_collision, bool close)
{
   // Pixel/thread mapping
   int x = blockIdx.x*blockDim.x + threadIdx.x;
   int y = blockIdx.y*blockDim.y + threadIdx.y;
   int offset = x + y * imageW; // 1D indexing

   if ((x >= imageW) || (y >= imageH)) return; // Check if within image bounds


   if (close) {
      d_output[offset].x = d_inp_arr[offset] > 0 ? 255 : 0;
      d_output[offset].y = d_inp_arr[offset] > 0 ? 255 : 0;
      d_output[offset].z = d_inp_arr[offset] > 0 ? 255 : 0;
      d_output[offset].w = 255;
      // *d_collision = true;
      float alpha = (arr_heading / 180.0f * CUDART_PI_F);
      float beta = (texobj_heading / 180.0f * CUDART_PI_F);
      float pos_from_left = x - imageW/2.0f;
      float pos_from_top = y - imageH/2.0f;
      float g_x = arr_pos_x + pos_from_left * cosf(alpha)
                            - pos_from_top  * sinf(alpha);
      float g_y = arr_pos_y + pos_from_left * sinf(alpha)
                            + pos_from_top  * cosf(alpha);

      float t_x = (g_x - texobj_pos_x) * cosf(-beta)
                  - (g_y - texobj_pos_y) * sinf(-beta) + imageW/2.0f;
      float t_y = (g_x - texobj_pos_x) * sinf(-beta)
                  + (g_y - texobj_pos_y) * cosf(-beta) + imageH/2.0f;


      float other_obj_dist = tex2D<float>(texobj, t_x / (float) imageW,
                                             t_y / (float) imageH);
      if (d_inp_arr[offset] > 0 && other_obj_dist > 0) {
         d_output[offset].x = 255;
         d_output[offset].y = 0;
         d_output[offset].z = 0;
         d_output[offset].w = 255;
         *d_collision = true;
      }
      else if (other_obj_dist > 0) {
         d_output[offset].x = 0;
         d_output[offset].y = 0;
         d_output[offset].z = 255;
         d_output[offset].w = 255;
      }
   }
   else {
      d_output[offset].x = d_inp_arr[offset] > 0 ? 255 : 0;
      d_output[offset].y = d_inp_arr[offset] > 0 ? 255 : 0;
      d_output[offset].z = d_inp_arr[offset] > 0 ? 255 : 0;
      d_output[offset].w = 255;
   }
}

////////////////////////////////////////////////////////////
/**********************  KERNEL END  **********************/
////////////////////////////////////////////////////////////

// For calling a function from a different file
extern "C" void render_kernel(dim3 gridSize, dim3 blockSize, uchar4 *d_output,
	                           int imageW, int imageH, float *inp_arr)
{
	float *d_inp_arr;

	checkCudaErrors(cudaMalloc((void **)&d_inp_arr,
                              (int) imageW * imageH * sizeof(float)));
	checkCudaErrors(cudaMemcpy(d_inp_arr, inp_arr,
                              imageW * imageH *sizeof(float),
	    	                     cudaMemcpyHostToDevice));

	// Launch render kernel
	g_render<<<gridSize, blockSize>>>(d_output, imageW, imageH, d_inp_arr);

   cudaFree(d_inp_arr);
}

extern "C" void render_collision_kernel(dim3 gridSize, dim3 blockSize,
                                        uchar4 *d_output, int imageW,
                                        int imageH, float *inp_arr1,
                                        float *inp_arr2,
                                        cudaTextureObject_t texobj,
                                        float arr_pos_x, float arr_pos_y,
                                        float arr_heading, float texobj_pos_x,
                                        float texobj_pos_y,
                                        float texobj_heading, bool *collision,
                                        bool close)
{
   bool *d_collision;
   checkCudaErrors(cudaMalloc((void **)&d_collision, sizeof(bool)));


   checkCudaErrors(cudaMemcpy(d_collision, collision, sizeof(bool),
                              cudaMemcpyHostToDevice));
   cudaArray *cuArray;
   cudaChannelFormatDesc channelDesc =
         cudaCreateChannelDesc(32, 0, 0, 0, cudaChannelFormatKindFloat);

   checkCudaErrors(cudaMallocArray(&cuArray, &channelDesc, imageW,
                                   imageH));


   // Copy to device memory some data located at address h_data
   // in host memory
   checkCudaErrors(cudaMemcpyToArray(cuArray, 0, 0, inp_arr2,
                                     imageW*imageH*sizeof(float),
                                     cudaMemcpyHostToDevice));

   // Specify texture
   struct cudaResourceDesc resDesc;
   memset(&resDesc, 0, sizeof(resDesc));
   resDesc.resType         = cudaResourceTypeArray;
   resDesc.res.array.array = cuArray;

   // Specify texture object parameters
   struct cudaTextureDesc texDesc;
   memset(&texDesc, 0, sizeof(texDesc));
   texDesc.addressMode[0]   = cudaAddressModeBorder;
   texDesc.addressMode[1]   = cudaAddressModeBorder;
   texDesc.filterMode       = cudaFilterModeLinear;
   texDesc.readMode         = cudaReadModeElementType;
   texDesc.normalizedCoords = 1;

   // Create texture object
   checkCudaErrors(cudaCreateTextureObject(&texobj, &resDesc, &texDesc, NULL));

   float *d_inp_arr;
   checkCudaErrors(cudaMalloc((void **)&d_inp_arr,
                              (int) imageW * imageH * sizeof(float)));
   checkCudaErrors(cudaMemcpy(d_inp_arr, inp_arr1,
                              imageW * imageH *sizeof(float),
                              cudaMemcpyHostToDevice));
   // Launch render kernel
   c_render<<<gridSize, blockSize>>>(d_output, imageW, imageH, d_inp_arr,
                                     texobj, arr_pos_x, arr_pos_y, arr_heading,
                                     texobj_pos_x, texobj_pos_y,
                                     texobj_heading, d_collision, close);

   checkCudaErrors(cudaMemcpy(collision, d_collision, sizeof(bool),
                              cudaMemcpyDeviceToHost));

   checkCudaErrors(cudaFree(d_collision));
   checkCudaErrors(cudaFree(d_inp_arr));
   checkCudaErrors(cudaFreeArray(cuArray));
   checkCudaErrors(cudaDestroyTextureObject(texobj));
}


