#ifndef SHAPE_H
#define SHAPE_H

typedef struct Shape {
   float pos_x;
   float pos_y;
   float width;
   float height;
   float heading;
   const char color;
} Shape;

typedef struct Point {
   float x;
   float y;
} Point;

typedef struct Vector {
   float x;
   float y;
} Vector;

typedef struct Line {
   Point P0;
   Point P1;
} Line;

typedef struct Segment {
   Point P0;
   Point P1;
} Segment;

Point*   my_get_verts_of_shape(Shape *s);
float    my_pt_dist(Point *p1, Point *p2);
Point*   my_get_closest_verts(Shape *s1, Shape *s2);
float    my_dot_prod(Vector *v1, Vector *v2);
float    my_norm(Vector *v);
Vector*  my_get_vec_diff(Vector *v1, Vector *v2);
float    my_vec_dist(Vector *v1, Vector *v2);
Point*   my_get_closest_pt_to_seg(Point *p, Segment *s);
float    my_closest_pt_to_seg_dist(Point *p, Segment *s);
Vector*  my_get_vec_from_pts(Point *p1, Point *p2);
void     my_free_vec(Vector **ss);
void     my_free_pt(Point **ss);
Point*   my_get_neigh_verts(Shape *s, Point *p);
Segment* my_get_closest_segs(Shape *s1, Shape *s2);
Point*   my_get_closest_pts(Shape *s1, Shape *s2);

#endif // SHAPE_H
