#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

void my_error(const char* msg)
{
   fprintf(stderr, "%s: %s\n", msg, strerror(errno));
   exit(1);
}

double mod2(double x, double y)
{
   return x > 0 ? fmod(x, y) : y + fmod(x, y);
}

int div_up(int a, int b)
{
    return (a % b != 0) ? (a / b + 1) : (a / b);
}
