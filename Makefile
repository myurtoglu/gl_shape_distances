uname_S := $(shell sh -c 'uname -s 2>/dev/null || echo not')

ifeq ($(uname_S), Linux)
	LDFLAGS += -L/usr/lib/x86_64-linux-gnu -lglut -lGL -lGLU -lGLEW
	LDFLAGS += -L/usr/local/cuda/lib64 -lcudart
endif

ifeq ($(uname_S), Darwin)
	FRAMEWORKS = -framework OpenGL -framework GLUT
	LDFLAGS += -L/usr/local/cuda/lib -lcudart -L/opt/local/lib -lGLEW
endif

NVCC=nvcc
NVCC_FLAGS      =  -m64 -cuda
INCLUDES        =  -I/usr/local/cuda/include -I/opt/local/include -I/usr/local/cuda/samples/common/inc
GENCODE_FLAGS   := -gencode arch=compute_20,code=sm_20
GENCODE_FLAGS   += -gencode arch=compute_30,code=sm_30
CPPFLAGS += -Wno-deprecated-declarations -Wno-unused-parameter -Wno-switch -Wno-unused-function
.PHONY: clean clobber all run

all: main.exe

run: main.exe
	./main.exe

main.exe: main.o util.o shape.o CUDA-OpenGL_Kernel.o
	$(CXX) $^ -o $@ $(LDFLAGS) $(FRAMEWORKS)

main.o: main.cpp
	$(CXX) $(CPPFLAGS) $(INCLUDES) $(CXXFLAGS) -c $^

CUDA-OpenGL_Kernel.cu.cxx: CUDA-OpenGL_Kernel.cu
	$(NVCC) $(NVCC_FLAGS) $(GENCODE_FLAGS) $(INCLUDES) -c $< -o $@

CUDA-OpenGL_Kernel.o: CUDA-OpenGL_Kernel.cu.cxx
	$(CXX) $(CPPFLAGS) $(INCLUDES) $(CXXFLAGS) -c $< -o $@

util.o: util.cpp
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $^

shape.o: shape.cpp
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $^

clean:
	rm -rf *.o *.cxx

clobber: clean
	rm -rf *.exe
